import { AUTH_LOGIN, AUTH_LOGOUT } from '../mutation-types'
import router from '../../router'

export default {
  namespaced: true,
  state: () => ({
    user: null

  }),
  mutations: {
    [AUTH_LOGIN] (state, payload) {
      state.user = payload
    },
    [AUTH_LOGOUT] (state) {
      state.user = null
    }
  },
  actions: {
    login ({ commit }, payload) {
      console.log(payload)
      const user = { name: 'Administrator', email: 'admin@user.com' }
      router.push('/')
      commit(AUTH_LOGIN, user)
    },
    logout ({ commit }, payload) {
      commit(AUTH_LOGOUT)
    }
  },
  getters: {
    isLogin (state, getter) {
      return state.user != null
    }
  }
}
